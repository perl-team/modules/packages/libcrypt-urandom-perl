libcrypt-urandom-perl (0.53-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.53.
  * Add debian/tests/pkg-perl/smoke-files to enable one more smoke tests.

 -- gregor herrmann <gregoa@debian.org>  Sat, 15 Feb 2025 22:25:46 +0100

libcrypt-urandom-perl (0.52-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.52.
  * Update years of upstream copyright.
  * Add new SECURITY.md file.
  * Declare compliance with Debian Policy 4.7.0.
  * Switch from arch:all to arch:any.
  * Add debian/tests/pkg-perl/smoke-skip.

 -- gregor herrmann <gregoa@debian.org>  Sat, 01 Feb 2025 00:37:42 +0100

libcrypt-urandom-perl (0.40-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.40.

 -- gregor herrmann <gregoa@debian.org>  Sat, 02 Mar 2024 02:50:48 +0100

libcrypt-urandom-perl (0.39-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.39.
  * Add debian/upstream/metadata.
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 4.6.2.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Fri, 27 Oct 2023 01:18:21 +0200

libcrypt-urandom-perl (0.36-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libcrypt-urandom-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 22 Oct 2022 01:03:25 +0100

libcrypt-urandom-perl (0.36-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from deprecated 8 to 12.
  * Set debhelper-compat version in Build-Depends.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 12 Jun 2022 22:33:52 +0100

libcrypt-urandom-perl (0.36-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sat, 02 Jan 2021 18:02:40 +0100

libcrypt-urandom-perl (0.36-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Sun, 28 Jun 2015 14:39:34 +0200

libcrypt-urandom-perl (0.35-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Alexandre Mestiashvili ]
  * Imported Upstream version 0.35
  * cme fix dpkg, bumped Standards-Version,
    removed not relevant to Debian part of description

 -- Alexandre Mestiashvili <alex@biotec.tu-dresden.de>  Tue, 26 May 2015 14:45:38 +0200

libcrypt-urandom-perl (0.34-1) unstable; urgency=low

  * Initial Release. (Closes: #702567)

 -- Alexandre Mestiashvili <alex@biotec.tu-dresden.de>  Fri, 08 Mar 2013 14:23:02 +0100
